# Graphics Experiments
Projects I did to learn WebGL and WebGL 2 in around 2018 to 2020.
May be polished and expanded on in the future using [WebGPU](https://developer.mozilla.org/en-US/docs/Web/API/WebGPU_API).

Hosted on my website [malus.zone](https://malus.zone) ([Repo](https://gitlab.com/Maluscat/malus.zone)).
It needs to be cloned or symlinked into the website's project root, after which it will work out of the box.
