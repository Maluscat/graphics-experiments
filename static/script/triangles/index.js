const defaultProps = {
  size: 50,
  color: '17469E',
  linemode: 'false',
  lightness: 8,
  threshold: 100
};
let lineMode = false;

let resizeTimeout = null;
let drawWidth;
let drawHeight;
let amountX;
let amountY;

boiler.setDimensions();
window.addEventListener('resize', generateWithTimeout);

function generateWithTimeout() {
  if (resizeTimeout) {
    clearTimeout(resizeTimeout);
  }
  resizeTimeout = setTimeout(generate, 50);
}

// ---- Color functions ----
function checkHex(hex) {
  const prefixed = hex.includes('#');
  const prefix = !prefixed ? '#' : '';
  if (!/[\dA-Fa-f]{3}|[\dA-Fa-f]{6}/.test(prefix + hex)) {
    return null;
  } else if (hex.length + !prefixed == 7) {
    return prefix + hex;
  } else if (hex.length + !prefixed == 4) {
    return prefix + hex;
  } else {
    return null;
  }
}
function checkRgb(rgb) {
  rgb = {
    r: parseInt(rgb.r),
    g: parseInt(rgb.g),
    b: parseInt(rgb.b)
  };
  if ( rgb.r >= 0 && rgb.r <= 255 && rgb.r != null
    && rgb.g >= 0 && rgb.g <= 255 && rgb.g != null
    && rgb.b >= 0 && rgb.b <= 255 && rgb.b != null
  ) {
    return rgb;
  } else {
    return null;
  }
}

function hexToRgb(hex) {
  const prefix = !hex.includes('#');
  if (hex.length + prefix == 7) {
    return {
      r: parseInt(hex.slice(1 - prefix, 3 - prefix), 16),
      g: parseInt(hex.slice(3 - prefix, 5 - prefix), 16),
      b: parseInt(hex.slice(5 - prefix, 7 - prefix), 16),
    };
  } else if (hex.length + prefix == 4) {
    return {
      r: parseInt(hex.slice(1 - prefix, 2 - prefix) + hex.slice(1 - prefix, 2 - prefix), 16),
      g: parseInt(hex.slice(2 - prefix, 3 - prefix) + hex.slice(2 - prefix, 3 - prefix), 16),
      b: parseInt(hex.slice(3 - prefix, 4 - prefix) + hex.slice(3 - prefix, 4 - prefix), 16),
    };
  } else {
    return null;
  }
}
function rgbToHex(rgb) {
  const hex = {
    r: parseInt(rgb.r),
    g: parseInt(rgb.g),
    b: parseInt(rgb.b)
  };
  const fill = {
    r: hex.r < 16 ? '0' : '',
    g: hex.g < 16 ? '0' : '',
    b: hex.b < 16 ? '0' : ''
  };
  return '#' + fill.r + (hex.r).toString(16) + fill.g + (hex.g).toString(16) + fill.b + (hex.b).toString(16);
}

// ---- URL search parameter functions ----
function handleSearchParams() {
  const values = {};
  if (location.search) {
    const params = new URL(location).searchParams;
    for (const [key, value] of params) {
      values[key] = value;
    }
  } else if (location.hash) { // Compatibility for old version that used hash links
    const params = location.hash.slice(1).split('#');
    for (const param of params) {
      const [key, value] = param.split('=');
      values[key] = value;
    }
    if (values.lightness) values.lightness = (parseInt(values.lightness) / ((amountX + amountY) * 3)) * 100;
    if (values.threshold) values.threshold = (parseInt(values.threshold) / (amountX + amountY)) * 100;
  }
  if (Object.keys(values).length > 0) {
    updateProperties(values);
    const pageName = location.pathname.slice(location.pathname.lastIndexOf('/') + 1);
    history.replaceState(null, '', pageName);
  }
}

// ---- WebGL functions ----
function generate() {
  clearTimeout(resizeTimeout);
  computeSizes();
  boiler.clearAll();
  boiler.setDimensions();
  writeBuffer();
  draw();
}

function computeSizes() {
  const screenWidth = boiler.canvas.clientWidth;
  const screenHeight = boiler.canvas.clientHeight;

  const triangleSize = parseInt(inputSize.value);
  // Compute how big the triangles need to be in order to fill the whole canvas
  drawWidth = triangleSize + ((screenWidth % triangleSize) / Math.floor(screenWidth / triangleSize));
  drawHeight = triangleSize + ((screenHeight % triangleSize) / Math.floor(screenHeight / triangleSize));
  amountX = Math.round(screenWidth / drawWidth);
  amountY = Math.round(screenHeight / drawHeight);

  gl.uniform1i(uAmountX, amountX);
  gl.uniform2f(uDrawDims, drawWidth, drawHeight);
  gl.uniformMatrix3fv(uProjection, false, [
    2 / screenWidth, 0, 0,
    0, -2 / screenHeight, 0,
    -1, 1, 1
  ]);
}

function writeBuffer() {
  // Setting the attribute to a single triangle as it will get drawn repeatedly in an instanced draw
  const positions = new Float32Array([
    0, 0,
    0, drawHeight,
    drawWidth, 0
  ]);
  // ARRAY_BUFFER had been bound to `buffer`, so this is going into `buffer`
  gl.bufferData(gl.ARRAY_BUFFER, positions, gl.STATIC_DRAW);
}

function draw(
  fixed = false,
  rgb = {
    r: inputsRGB[0].value,
    g: inputsRGB[1].value,
    b: inputsRGB[2].value
  }
) {
  const drawMethod = lineMode ? gl.LINE_LOOP : gl.TRIANGLES;

  GLBoiler.clearAll(gl);

  const lightness = (amountX + amountY) * 3 * (sliderLightness.value / 100);
  const threshold = (amountX + amountY) * (sliderThreshold.value / 100);

  gl.uniform3f(uColor,
    rgb.r / 255,
    rgb.g / 255,
    rgb.b / 255
  );

  gl.uniform1f(uLightness, lightness);
  gl.uniform1f(uThreshold, threshold);

  if (!fixed) {
    gl.uniform2f(uRand, Math.random(), Math.random());
    gl.uniform2f(uRand90, Math.random(), Math.random());
  }

  gl.drawArraysInstanced(drawMethod, 0, 3, amountX * amountY * 2);
}
